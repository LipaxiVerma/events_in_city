import 'dart:io';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
void main() {
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Event In City'),
    );
  }
}
class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}
class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(

        title: Text(widget.title),
      ),

      body:
      SingleChildScrollView(
        child: Column(
          children: [
            // new children/update will begin from here
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                  child:Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Image.asset('assets/image/ID.jpg'),
                      SizedBox(height: 15),
                      Text("Indian Designer's haat Raipur"),
                      const Divider(
                        color: Colors.black26,
                        height: 25,
                        thickness: 4,
                      ),
                      //SizedBox(height:15,),
                      IntrinsicHeight(
                        child: Row(
                          children: [
                            Text('📅   28 to 30 april'),
                            VerticalDivider(
                              color: Colors.black12,
                              thickness: 4, //thickness of divier line
                            ),

                            Text(' 🕒   on 11am to 8pm'),
                            VerticalDivider(
                              color: Colors.black12,
                              thickness: 4, //thickness of divier line
                            ),
                            Expanded(child
                                : Text("📌 Hotel Babylon Inn, Raipur(CG)", maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            )),


                          ],
                        ),
                      ),
                      const Divider(
                        color: Colors.black26,
                        height: 25,
                        thickness: 4,
                      ),
                    ],
                  )

              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Image.asset('assets/image/AS.jpg'),
                    SizedBox(height: 15),
                    Text('Arijit Singh Live in Raipur(CG)'),


                    const Divider(
                      color: Colors.black26,
                      height: 25,
                      thickness: 4,
                    ),
                    //SizedBox(height:15,),
                    IntrinsicHeight(
                      child: Row(
                        children: [
                          Text('📅   29th march'),
                          VerticalDivider(
                            color: Colors.black12,
                            thickness: 4, //thickness of divier line
                          ),

                          Text(' 🕒   on 7pm'),
                          VerticalDivider(
                            color: Colors.black12,
                            thickness: 4, //thickness of divier line
                          ),
                          Expanded(child
                              : Text("📌 Zora  In Raipur(CG)", maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          )),


                        ],
                      ),
                    ),
                    const Divider(
                      color: Colors.black26,
                      height: 25,
                      thickness: 4,
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Image.asset('assets/image/GG.jpg'),
                    SizedBox(height: 15),
                    Text('Gourav Gupta Live in Raipur'),


                    const Divider(
                      color: Colors.black26,
                      height: 25,
                      thickness: 4,
                    ),
                    //SizedBox(height:15,),
                    IntrinsicHeight(
                      child: Row(
                        children: [
                          Text('📅   29th march'),
                          VerticalDivider(
                            color: Colors.black12,
                            thickness: 4, //thickness of divier line
                          ),

                          Text(' 🕒   on 7pm'),
                          VerticalDivider(
                            color: Colors.black12,
                            thickness: 4, //thickness of divier line
                          ),
                          Expanded(child
                              : Text("📌 Zora  In Raipur(CG)", maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          )),


                        ],
                      ),
                    ),
                    const Divider(
                      color: Colors.black26,
                      height: 25,
                      thickness: 4,
                    ),
                  ],
                ),

              ),
            ),
            //3rd column
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                  child:Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Image.asset('assets/image/ID.jpg'),
                      SizedBox(height: 15),
                      Text("Indian Designer's haat Raipur"),


                      const Divider(
                        color: Colors.black26,
                        height: 25,
                        thickness: 4,
                      ),
                      //SizedBox(height:15,),
                      IntrinsicHeight(
                        child: Row(
                          children: [
                            Text('📅   28 to 30 april'),
                            VerticalDivider(
                              color: Colors.black12,
                              thickness: 4, //thickness of divier line
                            ),

                            Text(' 🕒   on 11am to 8pm'),
                            VerticalDivider(
                              color: Colors.black12,
                              thickness: 4, //thickness of divier line
                            ),
                            Expanded(child
                                : Text("📌 Hotel Babylon Inn, Raipur(CG)", maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            )),


                          ],
                        ),
                      ),
                      const Divider(
                        color: Colors.black26,
                        height: 25,
                        thickness: 4,
                      ),
                    ],
                  )

              ),
            ),
            // 4th column will start here

          ],
        ),
        // ending of the main container
      ),
    );
  }
}
